import argparse
import datetime
import json
from random import shuffle
import random
from typing import Dict

from utils.fingerprint_utils import editor_to_articles, EDITED, AUTHORED

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--articles_csv', '-a',
                        dest='articles_csv',
                        help='path for articles.csv',
                        default='../articles.csv')
    parser.add_argument('--editors_csv', '-e',
                        dest='editors_csv',
                        help='path for editors.csv',
                        default='../editors.csv')
    parser.add_argument('--no_validation',
                        dest='no_validation',
                        help="no validation set, build json including all manuscripts for each editor (for building production fingerprints)",
                        default=False,
                        action='store_true')
    args = parser.parse_args()


    m = editor_to_articles(editors_csv=args.editors_csv, articles_csv=args.articles_csv)

    m2: Dict = {}
    validation_set = []
    # we generate a validation set by stratified sampling 10% of edited articles per editor (minimum one)
    random.seed(42)
    editors = sorted(list(m.keys()))

    for editor in editors:
        articles = m.get(editor)
        edited = articles[EDITED]
        if len(edited) == 0:
            continue
        n_edited = len(edited)
        if args.no_validation:
            n_val = 0
        else:
            n_val = max(1, n_edited // 10)

        shuffle(edited)

        validation_set.extend([(manus, editor) for manus in edited[:n_val]])
        m2[editor] = {EDITED: edited[n_val:], AUTHORED: articles[AUTHORED]}

    timestamp = '{:%Y-%b-%d_%H-%M-%S}'.format(datetime.datetime.now())
    editor_to_articles_json = f"editor_to_articles{timestamp}.json"
    set_json = f"validation_set{timestamp}.json"
    with open(editor_to_articles_json, 'w') as file:
        json.dump(m2, file)
    print(f"dumped editor_to_articles to {editor_to_articles_json}")
    if not args.no_validation:
        with open(set_json, "w") as file:
            json.dump(validation_set, file)
        print(f"dumped {len(validation_set)} to {set_json}")
