import argparse
import glob
import json
import sys

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--jsonl',
                        dest='jsonl',
                        help='jsonl file"',
                        default='input.jsonl')
    args = parser.parse_args()
    total = {}
    i=0
    with open(args.jsonl, "r") as file:
        print(f"loading {args.jsonl}")
        for l in file:
            line = json.loads(l)
            editor_suggestions = line["editor_suggestions"][:50]
            total[line["manuscript_id"]] = editor_suggestions
            i += 1
            print(f"loaded {i} lines")
    output = args.jsonl[:-1]
    with open(output, "w") as file:
        json.dump(total, file)
    print(f"dumped {len(total)} to {output}")