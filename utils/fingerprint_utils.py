import csv
import gzip
import json
import sys
from typing import Dict, List, Any

from utils.concepts import get_sorted_concepts
from utils.csvtools import csv_to_map
from utils.s3 import ProcessFilesS3

EDITED: str = "EDITED"
AUTHORED: str = "AUTHORED"

def editor_to_articles(editors_csv: str,
                       articles_csv: str) -> Dict[str, Dict[str, List[str]]]:
    with open(editors_csv, 'r') as fp:
        editors = list(csv_to_map(csv_file=fp, key_index=0).keys())
    with open(articles_csv) as fp:
        articles = csv_to_map(csv_file=fp, key_index=0)
    result: Dict[str, Dict[str, List]] = {e: {EDITED: [], AUTHORED: []} for e in editors}
    for article, value in articles.items():
        editor = value.get("editorId", None)
        if not editor:
            continue
        ass = value["editorAssociation"]
        result[editor][ass].append(article)

    with open("editor_to_article.json", "w") as fp:
        json.dump(result, fp)
    return result


class ProcessEditors(ProcessFilesS3):

    def process_single_inner(self, s3object):
        pass

    def __init__(self, articles_csv: str):
        super().__init__()
        self.editors: Dict[str, List[Dict[str, Any]]] = {}

    @staticmethod
    def _concept_metrics_json(fn: str) -> Dict:
        if fn.endswith(".gz"):
            with gzip.open(fn, 'rb') as fp:
                return json.loads(fp.read().decode('utf-8'))
        with open(fn, 'r') as fp:
            return json.loads(fp.read())

    @staticmethod
    def _parse_concept_metrics_csv(fn: str) -> Dict:
        if fn.endswith(".gz"):
            fp = gzip.open(fn, 'rt')
        else:
            fp = open(fn, 'r')
        csv.field_size_limit(sys.maxsize)
        metareader = csv.reader(fp, doublequote=True, delimiter='\t')
        cm = {}
        line = 0
        header = ["display",
                  "is_valid",
                  "conceptKey",
                  "frequency",
                  "hits",
                  "rect_freq",
                  "rect_ratio",
                  "freq_max",
                  "mean_freq",
                  "supergrams",
                  "pmi",
                  "concentration",
                  "linkedEntities",
                  "keySpectrum"]  # the expected header
        excludes = ["keySpectrum"]
        mask = [i for i in range(len(header)) if header[i] not in excludes]
        masked_header = [header[i] for i in mask]
        for row in metareader:
            if line == 0:
                assert all([row[i] == header[i] for i in range(len(header))])
                print(f"header: {header}")
                line += 1
                continue
            if len(header) != len(row):
                print(f"line {line} length {len(row)}, expected {len(header)}")
                line += 1
                continue
            masked_row = [row[i] for i in mask]
            cm.update({row[2]: {h: r} for h, r in zip(masked_header, masked_row)})
        return cm

    def proces_inner(self, content: Dict, source_name: str):
        print(f"processing {content['doi']}")
        concepts = get_sorted_concepts(content)
        self.editors[content['doi']] = concepts[:10]