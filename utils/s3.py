import bz2
import csv
import glob
import gzip
import json
import os
import sys
import tempfile
from abc import abstractmethod, ABC, ABCMeta
from typing import Dict, List, Any, Tuple

import boto3 as boto3
from botocore.exceptions import ClientError


class ProcessFiles(metaclass=ABCMeta):

    @abstractmethod
    def proces_inner(self, content: Dict, source_name: str):
        pass


class ProcessFilesS3(ProcessFiles, ABC):

    def __init__(self):
        self.s3 = boto3.resource('s3')
        self.s3_client = boto3.client('s3')

    def process_single(self,
                       bucket_name: str,
                       key: str) -> Tuple[Dict, str]:
        bucket: boto3.resources.factory.s3.Bucket = self.s3.Bucket(bucket_name)
        filename = key.split('/')[-1]
        source_name = next(e for e in key.split('/') if
                           '.' in e)  # find a name containing a '.' as that is typically the source name, e.g ... plos/poc/articles/101.xml/plos/poc/v2/...
        content: Dict = self._get_content(filename=filename,
                                          source_name=source_name,
                                          bucket=bucket,
                                          key_name=key)
        return content, source_name

    def _get_content(self, filename: str, source_name: str, bucket, key_name:str):
        if filename.endswith('.gz'):
            tmp_file, tmp_path = tempfile.mkstemp(suffix=source_name + '_' + filename)
            try:
                with open(tmp_path, 'wb') as data:
                    self.s3_client.download_fileobj(bucket.name, key_name, data)
                with gzip.open(tmp_path, 'rb') as data:
                    return json.loads(data.read().decode('utf-8'))
            except OSError as ose:
                print(f"{str(ose)} s3://{bucket.name}/{key_name}")
            finally:
                os.close(tmp_file)
                if os.path.isfile(tmp_path):
                    os.remove(tmp_path)
        else:
            raise ValueError("not supported yet")

    def process(self,
                bucket_name: str = "poc-processed-unsilo",
                content_provider: str = "plos",
                content_collection: str = "poc",
                extra_prefix: str = 'editors',
                filename: str = 'document-concepts-pre.json.gz',
                version: str = 'v2'):
        bucket: boto3.resources.factory.s3.Bucket = self.s3.Bucket(bucket_name)
        count = 0
        prefix: str = f"{content_provider}/{content_collection}/{extra_prefix}"
        for obj in bucket.objects.filter(Prefix=prefix):
            key_name: str = obj.key
            if key_name.endswith(filename) and f"/{version}/" in key_name:
                source_name = key_name[len(prefix):key_name.index("/", len(prefix) + 1)].strip('/')
                content = self._get_content(filename=filename,
                                            source_name=source_name,
                                            bucket=bucket,
                                            key_name=key_name)
                self.proces_inner(content, source_name)
                count += 1
        print("Total: " + str(count))
