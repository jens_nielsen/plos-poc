import csv
from typing import Dict, List


def csv_to_map(csv_file, key_index: int, verbose:bool=False) -> Dict:
    metareader = csv.reader(csv_file, doublequote=True)
    line: int = 0
    header: List[str] = []
    _docs = {}
    for row in metareader:
        if line == 0:
            header = row
            if verbose:
                print(f"header: {header}")
                print(f"using {header[key_index]} for identifying articles")
            line += 1
            continue
        if len(header) != len(row):
            print(f"line {line} length {len(row)}, expected {len(header)}")
            line += 1
            continue
            #print(f"row: {row}")
        _doc: Dict = {k: v for k, v in zip(header, row)}
        article_id: str = row[key_index]
        _docs[article_id] = _doc
        line += 1
        if verbose and line % 10000 == 0:
            print(f"processed {line} lines")
    return _docs