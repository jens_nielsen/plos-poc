import argparse
import datetime
import json
import sys
from collections import Counter
from multiprocessing import Manager, Process, Queue
from multiprocessing import current_process
from queue import Empty
from random import shuffle
from typing import Dict, List

from botocore.exceptions import ClientError

from utils.concepts import sort_concepts, get_sorted_concepts, get_by_label, set_by_label
from utils.fingerprint_utils import editor_to_articles, AUTHORED, EDITED
from utils.s3 import ProcessFilesS3


class ProcessFilesS3Simple(ProcessFilesS3):
    def proces_inner(self, content: Dict, source_name: str):
        pass


def get_concepts(article_id_list: List[str],
                 processor: ProcessFilesS3,
                 sub_collection: str = "articles",
                 publication_date_decay: float = 0.5,
                 boost_repeated_concepts: bool = True,
                 source: str = "unknown") -> List:
    bucket_name = "poc-processed-unsilo"
    content = []
    for article_id in article_id_list:
        # TODO: save all conceps for each article along with their publication date
        key_name = f"plos/poc/{sub_collection}/{article_id}.xml/plos/poc/v2/document-concepts-pre.json.gz"
        try:
            article_content, _ = processor.process_single(bucket_name="poc-processed-unsilo", key=key_name)
        except ClientError as e:
            print(f"{str(e)} : s3://{bucket_name}/{key_name}")
            continue
        if article_content is None:
            print(f"content is None : s3://{bucket_name}/{key_name}")
            continue
        elif "concepts" not in article_content:

            print(f"content has no concepts, only {list(article_content.keys())} : s3://{bucket_name}/{key_name}")
            continue
        else:
            article_content = {
                "publicationDate": article_content["publicationDate"],
                "concepts": [{
                    "key": c["key"],
                    "display": c["display"],
                    "scores": c["scores"],
                    "source": source
                } for c in article_content["concepts"]]}

            content.append(article_content)

    decay_enabled = 1.0 > publication_date_decay > 0.0

    # now punish the old and promote the new
    if decay_enabled and len(content) > 1:
        apply_age_decay(content, publication_date_decay)

    if boost_repeated_concepts:
        boost_repeated(content=content)

    result = []
    for c in [a["concepts"] for a in content]:
        result.extend(c)

    return result


def boost_repeated(content):
    counter = Counter()
    concepts_by_key = {}
    for c in content:
        for concept in c["concepts"]:
            k = concept["key"]
            current = concepts_by_key.get(k, None)
            if current:
                current_score = get_by_label(current["scores"], "globalScore")
                concept_score = get_by_label(concept["scores"], "globalScore")
                if current_score < concept_score:
                    concepts_by_key[k] = concept
            else:
                concepts_by_key[k] = concept
            counter.update({k: 1})
    repetition_boost = [1.0, 1.25, 1.35, 1.5]
    for k, c in concepts_by_key.items():
        if len(content) > 1:
            repetitions = counter.get(k) - 1
            if repetitions > 3:
                repetitions = 3
            score: float = get_by_label(x=c["scores"], label="globalScore")
            score *= repetition_boost[repetitions]
            set_by_label(x=c["scores"], label="globalScore", value=score)
            c["repetitions"] = repetitions
            c["repetition_boos"] = repetition_boost[repetitions]


def apply_age_decay(content, publication_date_decay):
    publication_dates = [get_date(c) for c in content]
    if any(x is not None for x in publication_dates):
        most_rescent = max([pdate for pdate in publication_dates if pdate is not None])
        least_rescent = min([pdate for pdate in publication_dates if pdate is not None])
        publication_dates = [pdate if pdate is not None else least_rescent for pdate in publication_dates]
        if most_rescent > least_rescent:
            max_age = most_rescent - least_rescent
            factors: List[float] = [1 - (publication_date_decay * ((most_rescent - current) / max_age)) for current
                                    in publication_dates]
            for i in range(len(content)):
                decay: float = factors[i]
                c = content[i]
                for concept in c["concepts"]:
                    score: float = get_by_label(x=concept["scores"], label="globalScore")
                    score *= decay
                    set_by_label(x=concept["scores"], label="globalScore", value=score)
                    concept["publicationDate"] = content[i]["publicationDate"]
                    concept["age"] = str(most_rescent - publication_dates[i])
                    concept["age_decay"] = decay


def get_date(content: Dict) -> datetime:
    if not has_date(content):
        return None
    pdate_str: str = content["publicationDate"]
    try:
        dt = datetime.datetime.strptime(pdate_str, "%Y-%m-%d")
    except ValueError as v:
        print(f"{pdate_str} raised {str(v)}")
        return None
    return dt


def has_date(content: Dict) -> bool:
    return "publicationDate" in content and content["publicationDate"]


def producer(editor_list: List,
             editor_article_association: Dict,
             # editor_fingerprints_retval: Dict,
             result_queue: Queue,
             n_editor_concepts: int,
             n_edited_concepts: int,
             n_authored_concepts: int,
             timestamp: str):
    name: str = current_process().name
    total = len(editor_list)
    print(f"[{name}] start with {total} editors, {timestamp}")
    processor = ProcessFilesS3Simple()
    result = {}
    count = 0
    for editor in editor_list:
        editor_concepts_only = get_concepts(article_id_list=[editor],
                                            processor=processor,
                                            sub_collection="editors",
                                            publication_date_decay=-1,
                                            source="profile")
        editor_concepts = sort_concepts(editor_concepts_only)[:n_editor_concepts]
        authored_article_concepts = get_concepts(article_id_list=editor_article_association[editor][AUTHORED],
                                                 processor=processor,
                                                 sub_collection="articles",
                                                 publication_date_decay=0.5,
                                                 source="authored")
        editor_concepts.extend(sort_concepts(authored_article_concepts)[:n_authored_concepts])
        edited_article_concepts = get_concepts(article_id_list=editor_article_association[editor][EDITED],
                                               processor=processor,
                                               sub_collection="articles",
                                               publication_date_decay=0.5,
                                               source="edited")
        editor_concepts.extend(sort_concepts(edited_article_concepts)[:n_edited_concepts])
        r = {editor: editor_concepts}
        result.update(r)
        result_queue.put(r)
        count += 1
        if total < 10 or count % (total // 10) == 0:
            print(f"{name} {count}/{total} done")
    json_ = f"fingerprints_{name}_{timestamp}.json"
    with open(json_, "w") as file:
        json.dump(result, file)
    print(f"{name} dumped result to {json_}")
    # editor_fingerprints_retval.update(result)
    print(f"{name} stop, {count}/{total} done")


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--editor_to_articles', '-a',
                        dest='editor_to_articles',
                        help='path for editor_to_articles json',
                        default='../editor_to_articles2018-Oct-10_13-06-45.json')
    parser.add_argument('--n_cpu', '-n',
                        type=int,
                        dest='n_cpu',
                        default=2,
                        help="numper of processes to start")
    parser.add_argument('--max_items', '-m',
                        type=int,
                        dest='max_items',
                        default=-1,
                        help="max items to process (set nmegative to disable, disabled by default)")
    parser.add_argument('--n_editor_concepts',
                        type=int,
                        dest="n_editor_concepts",
                        default=10,
                        help="number of editor concepts to use in fingerprint")
    parser.add_argument('--n_edited_concepts',
                        type=int,
                        dest="n_edited_concepts",
                        default=10,
                        help="number of EDITED article concepts to use in fingerprint")
    parser.add_argument('--n_authored_concepts',
                        type=int,
                        dest="n_authored_concepts",
                        default=10,
                        help="number of AUTHORED article concepts to use in fingerprint")
    # parser.add_argument('--timestamp',
    #                     type=str,
    #                     dest="timestamp",
    #                     default="None",
    #                     help="timestamp")
    parser.add_argument('--timestamp',
                        type=str,
                        dest="timestamp",
                        default="None",
                        help="timestamp")

    args = parser.parse_args()
    timestamp = args.timestamp  # '{:%Y-%b-%d_%H-%M-%S}'.format(datetime.datetime.now())
    with open(args.editor_to_articles, "r") as file:
        m = json.load(file)

    result_queue = Queue()

    editors = list(m.keys())
    if args.max_items > 0:
        editors = editors[:args.max_items]

    n_cpu = min(args.n_cpu, len(editors))
    chunksize = len(editors) // (n_cpu)
    processes = []
    for i in range(n_cpu):
        start = i * chunksize
        stop = (i + 1) * chunksize
        if i + 1 == n_cpu:
            stop = len(editors)
        editor_list_i = editors[start:stop]
        editor_articles_i: Dict = {j: m[j] for j in editor_list_i}
        p = Process(target=producer, kwargs={"editor_list": editor_list_i,
                                             "editor_article_association": editor_articles_i,
                                             # "editor_fingerprints_retval": editor_fingerprints,
                                             "result_queue": result_queue,
                                             "n_editor_concepts": args.n_editor_concepts,
                                             "n_edited_concepts": args.n_edited_concepts,
                                             "n_authored_concepts": args.n_authored_concepts,
                                             "timestamp": timestamp})
        p.start()
        processes.append(p)
    name = current_process().name
    fingerprint_count = 0
    timeout_count = 0
    fingerprints_json = f"editor_fingerprints_{timestamp}.jsonl"
    while any(p.is_alive() for p in processes) or not result_queue.empty():
        try:
            fingerprint = result_queue.get(timeout=5)
        except Empty:
            running = sum(p.is_alive() for p in processes)
            timeout_count += 1
            print(f"{name} {timeout_count} timeout, {running} alive")
            continue
        timeout_count = 0
        with open(fingerprints_json, "a") as file:
            file.write(json.dumps(fingerprint) + "\n")
        del fingerprint
        fingerprint_count += 1
        print(f"{name} {fingerprint_count}/{len(editors)} fingerprints build")
    print(f"{name} done, build {fingerprint_count}/{len(editors)} fingerprints")
    print(f"editor fingerprints dumped to {fingerprints_json}")

    # processor = ProcessEditors(articles_csv=args.articles_csv)
    # processor.process()
    # with(open("editor_concepts.json", 'w')) as fp:
    #    json.dump(processor.editors, fp)
