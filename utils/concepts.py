from typing import List, Dict, Any


def sort_concepts(concepts: List, label: str = "globalScore") -> list:
    return sorted(concepts,
                  key=lambda x: get_by_label(x["scores"], label, 0.0),
                  reverse=True)


def get_sorted_concepts(content: Dict, label: str = "globalScore") -> List:
    return sort_concepts(content['concepts'], label=label)


def get_by_label(x: List[Dict[str, Any]], label: str, default: Any=None) -> Any:
    for element in x:
        if element["label"] == label:
            return element["value"]
    return default


def set_by_label(x: List[Dict[str, Any]], label: str, value:float) -> float:
    old_value: float = None
    for element in x:
        if element["label"] == label:
            old_value = element["value"]
            element["value"] = value
    return old_value