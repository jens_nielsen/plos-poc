import argparse
import datetime
import json
import time
from collections import Counter
from queue import Empty
from typing import Dict, List, Any, Tuple, Set

from botocore.exceptions import ClientError

from utils.concepts import get_sorted_concepts
from utils.csvtools import csv_to_map
from utils.s3 import ProcessFilesS3

from multiprocessing import Process, Queue, current_process, Value, Manager

import random
from random import shuffle


class ProcessManuscripts(ProcessFilesS3):

    def proces_inner(self, content: Dict, source_name: str):
        pass

    def __init__(self, fingerprints: Dict[str, List[Dict]]):
        super().__init__()

        # editor -> Dict of key -> concept
        self.editor_fingerprints: Dict[str, Dict[str, Dict]] = {e: {c["key"]: c for c in v} for e, v in
                                                                fingerprints.items()}
        # editor -> dict of (concept_key -> global_score)
        self.editors_concepts = {e: {x["key"]: get_by_label("globalScore", x["scores"]) for x in v} for e, v in
                                 fingerprints.items()}
        del fingerprints

    @staticmethod
    def _get_scores(scores: List[Dict], labels: Set[str]):
        return [e for e in scores if e["label"] in labels]

    def find_editors(self, bucket_name: str, key: str) -> List:
        document_content, _ = self.process_single(bucket_name=bucket_name, key=key)
        document_concepts = get_sorted_concepts(document_content)
        document_concept_scores = {c["key"]: get_by_label("globalScore", c["scores"]) for c in document_concepts[:100]}
        document_concepts_dict = {c["key"]: c for c in document_concepts[:100]}
        # dict of: editor -> (score, intersecting_concept_keys)
        editor_scores: Dict[str, Tuple[float, Set[str]]] = {}

        for editor_id, editor_concept_scores in self.editors_concepts.items():
            intersecting_concept_keys: Set = set(editor_concept_scores.keys()) & set(document_concept_scores.keys())
            score: float = sum(
                [editor_concept_scores[c] + document_concept_scores[c] for c in intersecting_concept_keys])
            editor_scores[editor_id] = (score, intersecting_concept_keys)

        _result = []
        included_scores: Set[str] = {"globalScore", "localScore"}
        for editor_id, (score, concept_keys) in sorted(editor_scores.items(), key=lambda x: x[1][0], reverse=True):
            if score == 0:
                break
            editor_concepts: Dict[str, Dict] = self.editor_fingerprints[editor_id]
            concepts = []
            for k in concept_keys:
                c = editor_concepts[k]
                c.update({"manuscriptScores": [ms for ms in document_concepts_dict[k]["scores"] if ms["label"] in included_scores]})
                concepts.append(c)
            entry = {"editorId": editor_id,
                     "score": score,
                     "concepts": concepts}
            _result.append(entry)
        return _result


def get_by_label(label: str, labeled_values: List[Dict[str, Any]], default=None) -> Any:
    for lv in labeled_values:
        if lv.get("label", None) == label:
            return lv.get("value", default)
    return default


def producer(fingerprints: str,
             jobs: Queue,
             result_queue: Queue):
    process_name = current_process().name
    print(f"{process_name} starting poducer")
    print(f"{process_name} loading {args.editors}")
    with open(fingerprints, 'r') as fp:
        editor_fingerprints = json.load(fp)
    processor = ProcessManuscripts(fingerprints=editor_fingerprints)
    produced = 0
    errors = 0
    bucket = "poc-processed-unsilo"
    while not jobs.empty():
        try:
            job = jobs.get(timeout=1)
        except Empty:
            print(f"{name} timeout on jobs queue")
            continue
        expected_editor, key_name, manuscript_id = job
        try:
            editor_suggestions = processor.find_editors(bucket_name=bucket, key=key_name)
        except (ClientError, ConnectionResetError, EOFError, OSError, AttributeError) as error:
            print(f"{process_name} {str(error)} s3://{bucket}/{key_name}")
            errors += 1
            continue
        suggestions = {"manuscript_id": manuscript_id,
                       "expectedEditor": expected_editor,
                       "editor_suggestions": editor_suggestions}
        result_queue.put((expected_editor,
                          suggestions))
        produced += 1
        if produced // 1000 == 0:
            print(f"{process_name} produced {produced} ({errors} errors)")
    print(f"{process_name} producer done - produced {produced} ({errors} errors)")


if __name__ == '__main__':
    # from utils import open_file_tracker
    # open_file_tracker.setup()
    parser = argparse.ArgumentParser()
    parser.add_argument('--editors', '-e',
                        dest='editors',
                        help='path for editor fingerprints in json',
                        default='../fingerprints_2018-Oct-11_08-57-31.json')
    parser.add_argument('--manuscripts',
                        dest='manuscripts',
                        default=None,
                        help='manuscripts (as list of ids in json) to assign editor to. exactly one of manuscripts and validation must be specified')
    parser.add_argument('--validation',
                        dest='validation',
                        default=None,
                        help='validation_set json file with pairs of editor. exactly one of manuscripts and validation must be specified')
    parser.add_argument('--sub_collection',
                        dest="sub_collection",
                        help="for plos poc we had articles, manuscripts and editors as valid sub collections, set to None to disable",
                        default="articles")
    parser.add_argument('--n_cpu', '-n',
                        type=int,
                        dest='n_cpu',
                        default=2,
                        help="numper of result-producer processes to start")
    parser.add_argument('--max_items', '-m',
                        type=int,
                        dest='max_items',
                        default=-1,
                        help="max items to process (set nmegative to disable, disabled by default)")
    parser.add_argument("--timestamp",
                        type=str,
                        default="None",
                        help="timestamp to use for files")
    args = parser.parse_args()

    manuscripts = args.manuscripts
    validation = args.validation
    if not ((manuscripts is None) != (validation is None)):
        print("please specify exactly one of manuscripts or validation")
        exit()

    if args.sub_collection is None:
        sub_collection = ""
    else:
        sub_collection = args.sub_collection + "/"

    if manuscripts:
        print(f"loading {manuscripts}")
        with open(manuscripts) as file:
            manuscript_ids = json.load(file)
            all_manuscripts = [
                (
                "unknown", f"plos/poc/{sub_collection}{article}.xml/plos/poc/v2/document-concepts-pre.json.gz", article)
                for article in manuscript_ids]
    else:
        print(f"loading {validation}")
        with open(validation) as file:
            validation_ids = json.load(file)
            all_manuscripts = [
                (ed, f"plos/poc/{sub_collection}{article}.xml/plos/poc/v2/document-concepts-pre.json.gz", article)
                for (article, ed) in validation_ids]

    # with open(args.articles, 'r') as fp:
    #    article_editor_map = csv_to_map(fp, 0)

    n_cpu = args.n_cpu
    max_items = args.max_items

    # list of triple: (expected_editor, keyn_name, article_id)
    if args.max_items > 0:
        all_manuscripts = all_manuscripts[:max_items]
    result_queue = Queue()
    jobs_q = Queue(maxsize=len(all_manuscripts) + 1)
    suggestion_queue = Queue()
    print("adding jobs")
    i = 0
    for j in all_manuscripts:
        jobs_q.put(j)
        i += 1
        if i % 100 == 0:
            print(f"added {i} jobs")
    print(f"done adding {len(all_manuscripts)} jobs")
    producers: List[Process] = []
    for i in range(n_cpu):
        producer_kwargs = {
            "fingerprints": args.editors,
            "jobs": jobs_q,
            "result_queue": result_queue
        }
        producers.append(Process(target=producer, kwargs=producer_kwargs))
    for p in producers:
        p.start()

    result_count = 0
    expected_result_count = len(all_manuscripts)
    name = current_process().name
    timeout_s = 1
    timestamp = args.timestamp#'{:%Y-%b-%d_%H-%M-%S}'.format(datetime.datetime.now())
    suggestions_file = f"suggestions-{timestamp}.jsonl"
    print(f"{name} will dump to {suggestions_file}")
    while any([p.is_alive() for p in producers]) or (not result_queue.empty()):
        try:
            editor, result = result_queue.get(timeout=timeout_s)
        except Empty:
            timeout_s += 1
            if timeout_s > 5:
                print(f"{name} got {timeout_s} consecutive timeouts")
            continue
        with open(suggestions_file, "a") as file:
            file.write(json.dumps(result) + "\n")
        timeout_s = 1
        result_count += 1
        if expected_result_count < 100 or result_count % (expected_result_count // 100) == 0:
            print(f"{name} consumed {result_count}/{expected_result_count}")
    # kill processes:
    for p in producers:
        if p.is_alive():
            print(f"{name} killing {p.name}")
            p.terminate()
            p.join()
    print(
        f"{name} cosumer done {result_count}/{expected_result_count} ({expected_result_count-result_count} missing)")
