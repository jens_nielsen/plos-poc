#!/bin/bash
source ~/plos-poc/venv/bin/activate
export PLOSPOCPATH="${HOME}/plos-poc/"
export PYTHONPATH=$PLOSPOCPATH
python ${PLOSPOCPATH}editor_finder/find_editors.py --editors ../fingerprints.json --validation ../validation_set2018-Oct-11_13-58-42.json -n 16 -t 20 > out.txt 2> error.txt &&
resultfile=`ls -atr result*json | tail -n1` &&
suggestfile=`ls -atr suggest*jsonl | tail -n1` &&
aws s3 cp out.txt s3://unsilo-staff/jn/plos-poc/ &&
aws s3 cp $resultfile s3://unsilo-staff/jn/plos-poc/ &&
aws s3 cp $suggestfile s3://unsilo-staff/jn/plos-poc/ &&
sudo halt
