#!/bin/bash
PLOSPOCPATH=${HOME}/plos-poc/
export PYTHONPATH=$PLOSPOCPATH
source ${PLOSPOCPATH}/venv/bin/activate

editor_to_articles=${PLOSPOCPATH}editor_to_articles2018-Oct-11_13-58-42.json
validation=${PLOSPOCPATH}validation_set2018-Oct-10_13-06-45.json

timestamp=$(date +%Y-%m-%d_%H-%M-%S)
out=out_fingerprints_${timestamp}.txt
error=error_fingerprints_${timestamp}.txt
python ${PLOSPOCPATH}utils/build_editor_fingerprints.py --editor_to_articles ${editor_to_articles} --timestamp ${timestamp} -n 16  > ${out} 2> ${error}

if [ $? -eq 0 ]; then
	fingerprints=editor_fingerprints_${timestamp}.jsonl
	aws s3 cp $fingerprints s3://unsilo-staff/jn/plos-poc/
fi

aws s3 cp ${out} s3://unsilo-staff/jn/plos-poc/
aws s3 cp ${error} s3://unsilo-staff/jn/plos-poc/
#sudo halt
