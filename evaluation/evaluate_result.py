import argparse
import json
from typing import Dict

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', '-i',
                        dest='input',
                        help='path for result.json')
    parser.add_argument('--suggestions', '-j',
                        dest='suggestions',
                        help='path for suggestions.jsonl ')
    args = parser.parse_args()


    with open(args.input, 'r') as file:
        result: Dict = json.load(file)

    for i in range(len(result['tp'])):
        tp, fp, fn = result['tp'][i], result['fp'][i], result['fn'][i]
        sum_tp = sum(tp.values())
        sum_fp = sum(fp.values())
        sum_fn = sum(fn.values())
        rec = sum_tp / (sum_fn + sum_tp)
        pre = sum_tp / (sum_fp + sum_tp)
        print(f"@{i+1}")
        print(f"precision : {pre:.2}\ttp {sum_tp}, fp {sum_fp}, fn {sum_fn}")
        print(f"recall    : {rec:.2}")
        editors = set(tp.keys()) | set(fp.keys()) | set(fn.keys())
        recall = {e: tp.get(e, 0) / (fn.get(e, 0) + tp.get(e, 0)) for e in editors if e in fn or e in tp}
        precision = {e: tp.get(e, 0) / (fp.get(e, 0) + tp.get(e, 0)) for e in editors if e in fp or e in tp}
        all = recall.keys() & precision.keys()
        avg_recall = sum([recall[e]for e in all])/len(all)
        avg_precision = sum([precision[e] for e in all])/len(all)