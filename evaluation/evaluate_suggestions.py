import argparse
import gzip
import json
from collections import Counter
from typing import Dict, List

class Evaluator:

    def __init__(self):

        self.counters: List[Counter] = []
        for i in range(50):
            self.counters.append(Counter())

        self.mean_avg_prec_sum = 0
        self.not_suggested = 0
        self.total = 0
        self.rank_when_suggested = Counter()

    def eval(self, entry):
        self.total += 1
        suggested_editors = [s["editorId"] for s in entry["editor_suggestions"]]
        if len(suggested_editors) > len(self.counters):
            diff = len(suggested_editors) - len(self.counters)
            self.counters.extend([Counter() for i in range(diff)])

        expected_editor = entry["expectedEditor"]
        if expected_editor in suggested_editors:
            rank = suggested_editors.index(expected_editor) + 1
            self.rank_when_suggested.update(f"{rank}")
            self.mean_avg_prec_sum += 1 / (rank * rank)
        else:
            self.not_suggested += 1
        tp = 0
        fp = 0
        fn = 1
        for i in range(len(self.counters)):
            if len(suggested_editors) > i:
                if suggested_editors[i] == expected_editor:
                    tp = 1
                    fn = 0
                else:
                    fp += 1
            self.counters[i].update({"fp": fp, "fn": fn, "tp": tp})

    def print(self):
        recalls = [c["tp"] / (c["tp"] + c["fn"]) for c in self.counters]  # if c["tp"] > 0 or c["fp"] > 0]
        precisions = [c["tp"] / (c["tp"] + c["fp"]) for c in self.counters]  # if c["tp"] > 0 or c["fn"] > 0]

        mean_avg_prec = self.mean_avg_prec_sum / self.total
        print(f"mean avg precision, {mean_avg_prec}")
        print(f"total manuscripts, {self.total}")
        print(f"actual editor suggested, {self.total - self.not_suggested}")
        avg_rank_ws = sum(int(r)*c for r, c in self.rank_when_suggested.items()) / sum(self.rank_when_suggested.values())
        print(f"average rank when suggested, {avg_rank_ws}")
        print("topN,avg. recall,avg. precision")
        for (i, (r, p)) in enumerate(zip(recalls, precisions)):
            print(f"{i},{r:.4f},{p:.4f}")
            if r == 1.0:
                break
        print("\n")
        print("rank when suggested,count")
        for k,v in sorted(self.rank_when_suggested.items()):
            print(f"{k},{v}")
        # print("      top :" + " ".join([f"{x}".rjust(6) for x in range(len(recalls))]))
        # print("   recall :" + " ".join([f"{x:.4f}".rjust(6) for x in recalls]))
        # print("precision :" + " ".join([f"{x:.4f}".rjust(6) for x in precisions]))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--input', '-i',
                        dest='input',
                        help='path for suggestions.jsonl')
    args = parser.parse_args()
    evaluator = Evaluator()
    input_jsonl = args.input
    if input_jsonl[-3:] == ".gz":
        with gzip.open(input_jsonl, 'tr') as file:
            for l in file:
                content = file.read().decode('utf-8')
                evaluator.eval(json.loads(content))
    else:
        with open(args.input, 'r') as file:
            for l in file:
                evaluator.eval(json.loads(l))
    evaluator.print()

    #import matplotlib.pyplot as plt

    #plt.plot(recalls, precisions)
    #plt.show(block=True)



